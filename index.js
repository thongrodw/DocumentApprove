//Package
const express = require("express");
const ejs = require("ejs")
const session = require('express-session')
var mongoose = require('mongoose')
const bodyParser = require('body-parser')

const PORT = 8080;

//middleware
const app = express();
app.set('view-engine', 'ejs')
app.use(express.urlencoded({extended: false}))
app.use(bodyParser.json());

//MongoDB
const db = require("./config/db")

//Authentication
const authen = require('./config/authen');
var authenResult = authen.result
var loginStatus =  authen.loginStatus
var approveResult = authen.approve

//route
app.get("/home", function(req,res){
    res.send("Welcome")
})

app.get("/login",function(req,res){    //login
    res.render('index.ejs')
})

app.get("/",authenResult,function(req,res){    //to enter home require login
})

app.post("/",authenResult,function(req,res){    //to enter home require login
    res.redirect("/doc")
})

app.get("/doc",loginStatus,function(req,res){
    res.render("doc.ejs")
})

app.post("/doc",approveResult,function(req,res){
    res.redirect("/login")
})

//Listen
app.listen(PORT, console.log('Listen on Port:8080'))

exports.port = PORT
module.exports = app;